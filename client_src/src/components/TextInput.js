import React, { Component } from 'react';
// import TextDisplay from './TextDisplay'
import actions from '../redux/actions';
import axios from 'axios';

class TextInput extends Component {
  // getInitialState(){
  //     return {
  //         inputText: 'initial text'
  //     }
  // }
  constructor(props, context) {
    super(props, context);
    this.state = {
      inputText: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


 addTodo(newTodo) {
  axios.request({
    method: 'post',
    url: 'http://localhost:3000/api/todos',
    data: newTodo,
  })
    .then(response => {
      const todo = newTodo.todo
        // return people;
      this.props.dispatch(actions.addTodo(todo));
      // this.props.dispatch(actions.addTodo(this.state.inputText));
      // console.log(people);
    }).catch(err => console.log(err));
      // dispatch(getUsersAsync(people));
}

  handleChange(event) {
    // console.log("change");
    // console.log(event.target.value);
    this.setState({
      inputText: event.target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const newTodo = {
      todo: this.state.inputText,
      category: this.props.todos[this.props.selectedID].name
    }
    // console.log("submit button is woking?");
    if (this.state.inputText !== '') {
    // console.log("no input");
      // this.props.dispatch(actions.addTodo(this.state.inputText));
      this.addTodo(newTodo);
      this.setState({
        inputText: '',
      });
    }
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input className="new-todo" type="text" placeholder="Enter your Task here" value={this.state.inputText} onChange={this.handleChange} />
          {/* <input type="Submit" /> */}
          {/* <TextDisplay text={this.state.inputText}/> */}
        </form>
      </div>
    );
  }
}

export default TextInput;
